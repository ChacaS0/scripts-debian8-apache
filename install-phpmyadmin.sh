#!/bin/bash

echo ""
echo "       _          ___  ___       ___      _           _       "
echo "      | |         |  \/  |      / _ \    | |         (_)      "
echo " _ __ | |__  _ __ | .  . |_   _/ /_\ \ __| |_ __ ___  _ _ __  "
echo "| '_ \| '_ \| '_ \| |\/| | | | |  _  |/ _\` | '_ \` _ \| | '_ \ "
echo "| |_) | | | | |_) | |  | | |_| | | | | (_| | | | | | | | | | |"
echo "| .__/|_| |_| .__/\_|  |_/\__, \_| |_/\__,_|_| |_| |_|_|_| |_|"
echo "| |         | |            __/ |                              "
echo "|_|         |_|           |___/                               "
echo ""

echo "$(date +%F_%H-%M-%S)    | Installing phpMyAdmin..."
apt-get install -y phpmyadmin
echo "$(date +%F_%H-%M-%S)    | Installation complete"

# Config file phpMyAdmin :      /etc/phpmyadmin/config.inc.php


echo "$(date +%F_%H-%M-%S)    | Setting up phpMyAdmin..."
 
ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin


echo "$(date +%F_%H-%M-%S)    | Setting up access to phpMyAdmin..."
read -p '$(date +%F_%H-%M-%S)    | Input the IP address which will have access to phpMyAdmin: ' allowedIp
# cat <<EOF > /var/www/html/phpmyadmin/.htaccess
# order allow,deny
# allow from ${allowedIp}
# EOF

cat <<EOF > /var/www/html/phpmyadmin/.htaccess
AuthType Basic
AuthName "Restricted Files"
AuthUserFile /etc/apache2/auth/.htpasswd_phpmyadmin
Require valid-user
EOF

# Generate new allowed accounts to access
echo "$(date +%F_%H-%M-%S)    | Creating new allowed accounts to access..."
read -p '$(date +%F_%H-%M-%S)    | Username: ' username
htpasswd -cB /etc/apache2/auth/.htpasswd_phpmyadmin $username
echo "$(date +%F_%H-%M-%S)    | To add other accounts, use htpasswd -B /etc/apache2/auth/.htpasswd_phpmyadmin [USERNAME]"

# Force SSL
# cat "\$cfg['ForceSSL'] = 'true';" >> /etc/phpmyadmin/config.inc.php

echo "$(date +%F_%H-%M-%S)    | Reloading Apache..."
systemctl reload apache2

echo "$(date +%F_%H-%M-%S)    | phpMyAdmin installed"
